using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FinalTestChar.Models;

    public class LaZuittContext : DbContext
    {
        public LaZuittContext (DbContextOptions<LaZuittContext> options)
            : base(options)
        {
        }

        public DbSet<FinalTestChar.Models.Course> Course { get; set; }
        public DbSet<FinalTestChar.Models.Student> Student { get; set; }
        public DbSet<FinalTestChar.Models.Enroll> Enroll { get; set; }
}
