﻿using System;
namespace LaZuittEcommerce.Models;

public class OrderHistory
{
    public int Id { get; set; }
    public string FullName { get; set; }
    public string Billing { get; set; }
    public DateTime DateEnrolled { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }
}

