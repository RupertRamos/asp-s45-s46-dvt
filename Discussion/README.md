# Session Objectives
At the end of the session, the students are expected to:
* Understand the target framework .NET 6 version
* Create Data Seeding and Migration
* Create database model relationships
* Implement Session Extensions in the .NET 6

# Resources

## Instructional Materials
* [GitLab Repository](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/tree/main/11_Ecommerce_integration/Discussion/LaZuittEcommerce/LaZuittEcommerce)
* [Google Slide Presentation (Part I)](https://docs.google.com/presentation/d/1K0mF9cHbiHHssQBuiKpADhJXKNCvYeFi_-Om0Bz7xxQ/edit)
* [Google Slide Presentation (Part II)](https://docs.google.com/presentation/d/11FP9P_8XPmjkBYhsh-xM0_S2XYRUOgRGOH90r2aOnsE/edit#slide=id.p)

## Supplemental Materials
* [Shopping Cart Tutorial - YouTube](https://www.youtube.com/watch?v=sX3g6hQZ8Lw)
* [Session Extension](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/app-state?view=aspnetcore-7.0)
* [DbContext Lifetime, Configuration, and Initialization](https://learn.microsoft.com/en-us/ef/core/dbcontext-configuration/)
* [How to Join 3 Tables (or More) in SQL](https://learnsql.com/blog/how-to-join-3-tables-or-more-in-sql/)

# Session Proper

# ASP.NET 6 MVC with Simple E-commerce Integration  

## **1. Project Configuration**

Create your Web Applicaton (MVC) project. 

**Target Framework** should be **.NET 6** and in Advance settings, leave **Configure for HTTPS** and **Do not use top-level statements** unchecked.

![](/11_Ecommerce_integration/Discussion/readme-images/1-setup/1-project-setup.png)

Let's install the following dependencies by going to your **Nuget Package - Solution**.

* Microsoft.EntityFrameworkCore
* Microsoft.EntityFrameworkCore.Design
* Microsoft.EntityFrameworkCore.Sqlite
* Microsoft.EntityFrameworkCore.Tools
* Microsoft.VisualStudio.Web.CodeGeneration.Design

Your solution explorer under **Dependencies/Nuget** should generate these files.

![](/11_Ecommerce_integration/Discussion/readme-images/1-setup/2-nuget-packages.png)

**Optional** - Delete the following files/folder:
* **Controllers/HomeController.cs**
* **Views/Home** (entire Home folder)

Now, we're going to create our **Course** and **Cart** controllers and views.

Let's **Add New Scaffolding** and select **MVC Controller - Empty**. 

Name your files **CourseController** and **Cart Controller**.

![](/11_Ecommerce_integration/Discussion/readme-images/1-setup/3-scaffolding.png)

Now in the **Views** folder, create your folders and name them **Course** and **Cart**.

Create a razor view file on each folders and name it as **Index.cshtml**.

![](/11_Ecommerce_integration/Discussion/readme-images/1-setup/4-razor-views.png)

Your solution explorer structure should look like the image below.

![](/11_Ecommerce_integration/Discussion/readme-images/1-setup/5-solution-explorer.png)

Let's organize the design for our Index page.

Go to **Views/Shared/_Layout.cshtml** and edit the following code

![](/11_Ecommerce_integration/Discussion/readme-images/1-setup/6-layout-view.png)

**Title**
```html
<title>@ViewData["Title"] - LaZuitt Ecommerce</title>
```

**Navigation Bar**
```html
<div class="container">
    <a class="navbar-brand" asp-area="" asp-controller="Course" asp-action="Index">Course</a>
    <a class="navbar-brand" asp-area="" asp-controller="Cart" asp-action="Index">Cart</a>
</div>
```

**Footer**
```html
<div class="container">
    &copy; 2022 - LaZuitt Ecommerce
</div>
```

The last thing we need to modify is our code in **Program.cs**.

```javascript
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Course}/{action=Index}/{id?}");
```

Replace **Home** to **Course** so whatever the content from **Course/Index.cshtml** will be set as our default Home Page.

Run the program and should be a blank page and links for **Course** and **Cart** will not return any content for now.

![](/11_Ecommerce_integration/Discussion/readme-images/1-setup/7-empty-index.png)

## **2. Models Creation**

In our E-commerce Web Application, we will be needing these models:
* **Course** - This table serves as our products offered.
* **Student** - This table serves as our customers.
* **Enroll** - This table facilitates the enrollment date of our student.
* **EnrollCourses** - This is our intersection table that facilitates the 1:M relationship of student and enrolled courses.

Create classes for **Course**, **Student**, **Enroll** and **EnrollCourses**.

Creating our models in the **.NET 6 MVC** results to the following warnings, just like the image below.

![](/11_Ecommerce_integration/Discussion/readme-images/2-models/1-nullable-error.png)

If you hover through it, you'll get a message *Property 'propertyName' must contain a non-null value when exiting constructor. Consider declaring property as nullable.*

Declare the property as null.

```javascript
 public string Title { get; set; } = null!;
```

Or **Recommended** is you can look for your **ProjectName.csproj** file and on the Nullable tag, replace enable to **disable**.

![](/11_Ecommerce_integration/Discussion/readme-images/2-models/2-nullable-disable.png)

The **LaZuittEcommerce.csproj** is inside your Namespace folder, you can edit it with a text editor and the warnings will disappear.

### Models for our Database Schema

**Models/Course.cshtml** 

```cs
using System;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Linq;

namespace LaZuittEcommerce.Models; 

public class Course
{
    [Key]
    public int Id { get; set; }

    [Required]
    public string Title { get; set; }

    [Required]
    public string Description { get; set; }

    [Required]
    [Column(TypeName = "decimal(6,2)")]
    public decimal Price { get; set; }

    [Display(Name = "Unit")]
    public int Quantity { get; set; }

    [Required]
    [DataType(DataType.ImageUrl)]
    public string Logo { get; set; }
}
```

**Models/Student.cshtml**
```cs
using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace LaZuittEcommerce.Models;

public class Student
{
    [Key]
    public int Id { get; set; }

    [Required]
    [Display(Name = "First Name")]
    public string FirstName { get; set; }

    [Required]
    [Display(Name = "Last Name")]
    public string LastName { get; set; }

    [Required]
    public string Email { get; set; }

    [Required]
    [Display(Name = "Billing Address")]
    public string BillingAddress { get; set; }
}
```

**Models/Enroll.cshtml**
```cs
using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace LaZuittEcommerce.Models;

public class Enroll
{
    [Key]
    public int Id { get; set; }

    public int StudentId { get; set; }
    public virtual Student Student { get; set; }

    [Display(Name = "Enroll Date")]
    [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
    [DataType(DataType.Date)]
    public DateTime EnrollDate { get; set; }
}
```

**Models/EnrollCourses.cshtml**
```cs
using System;
using System.ComponentModel.DataAnnotations;

namespace LaZuittEcommerce.Models;

public class EnrollCourses
{
    [Key]
    public int Id { get; set; }

    public int EnrollId { get; set; }
    public virtual Enroll Enroll { get; set; }

    public int CourseId { get; set; }
    public virtual Course Course { get; set; }

    public int Quantity { get; set; }
}
```

### Models for our Cart 

Create a class and name it as **CartItem.cs**

**Models/CartItem.cs**

```cs
using System;

namespace LaZuittEcommerce.Models;

public class CartItem
{
    public int CourseId { get; set; }
    public string Icon { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }

    public decimal TotalAmount
    {
        get { return Quantity * Price; }
    }

    public CartItem()
    {
    }

    public CartItem(Course course)
    {
        CourseId = course.Id;
        Icon = course.Logo;
        Title = course.Title;
        Description = course.Description;
        Price = course.Price;
        Quantity = 1;
    }
}
```

This class will be used as temporary storage or like a session that stores objects in a list that can be accessed by index and we can perform operations on the list like searching, adding, updating and deleting. 

Under the **Views** folder, create a folder called **ViewModels** and inside this folder, create a class and name it as **CartViewModel**.

```cs
using System;
namespace LaZuittEcommerce.Models.ViewModels;

public class CartViewModel
{
    public List<CartItem> CartItems { get; set; }
    public decimal TotalAmount { get; set; }
}
```

The **CartViewModel** will be used in our Cart view page and is derived from **CartItem** model. This model displays all the information stored in **CartItem** model, every changes/updates on the list. 

Our current **Models** folder structure should look like the image below.

![](/11_Ecommerce_integration/Discussion/readme-images/2-models/3-model-structure.png)

## 3. Database Context and Data Seeding

### Database Context

**DbContext** is a bridge between your domain or entity classes and the database. **DbContext** is the primary class that is responsible for interacting with the database. 

In our namespace, we need to create a folder called **Data** and create a class and name it as **LaZuittContext**.

Let's create our database context

```cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using LaZuittEcommerce.Models;

namespace LaZuittEcommerce.Data;

public class LaZuittContext : DbContext
{
    public LaZuittContext(DbContextOptions<LaZuittContext> options) : base(options)
    {
    }

    public DbSet<Course> Course { get; set; }
    public DbSet<Student> Student { get; set; }
    public DbSet<Enroll> Enroll { get; set; }
    public DbSet<EnrollCourses> EnrollCourses { get; set; }
}
```

### Data Seeding

**Data seeding** is the process of populating a database with an initial set of data.

Now, under **Data** folder, create a class and name it as **SeedData**.

```cs
using Microsoft.EntityFrameworkCore;
using LaZuittEcommerce.Models;

namespace LaZuittEcommerce.Data;

public class SeedData
{
    public static void SeedDatabase(LaZuittContext context)
    {
        context.Database.Migrate();

        if (!context.Course.Any())
        {
            context.Course.AddRange(
                new Course
                {
                    Title = "ASP.NET",
                    Description = "ASP.NET is an open source web framework, created by Microsoft, for building modern web apps and services that run on macOS, Linux, Windows, and Docker.  ",
                    Price = 17000,
                    Quantity = 1,
                    Logo = "/images/aspnet-logo.png"
                },
                new Course
                {
                    Title = "Django",
                    Description = "Django is a free and open-source, Python-based web framework that follows the model–template–views architectural pattern.",
                    Price = 19000,
                    Quantity = 1,
                    Logo = "/images/django-logo.png"
                },

                new Course
                {
                    Title = "Laravel",
                    Description = "Laravel is a free and open-source PHP web framework, created by Taylor Otwell and intended for the development of web applications following the model–view–controller architectural pattern",
                    Price = 16000,
                    Quantity = 1,
                    Logo = "/images/laravel-logo.png"
                },
                new Course
                {
                    Title = "Node.js",
                    Description = "Node.js is an open-source, cross-platform, back-end JavaScript runtime environment that runs on a JavaScript Engine and executes JavaScript code outside a web browser",
                    Price = 14000,
                    Quantity = 1,
                    Logo = "/images/nodejs-logo.png"
                }
            );

            context.SaveChanges();
        }
    }
}
```

This line of code applies any pending migrations for the context to the database. It creates your database if it doesn't exist.

```c++
context.Database.Migrate();
```

The **if** condition checks if there's no **Any()** record. If true then it populates our Course table. 

```c++
if (!context.Course.Any())
```

To perform the Data Seeding, we need to add few line of codes in our **Program.cs** like the image shown below.

![](/11_Ecommerce_integration/Discussion/readme-images/3-context-seed/2-seed-program-code.png)

```c++
var context = app.Services.CreateScope().ServiceProvider.GetRequiredService<LaZuittContext>();
SeedData.SeedDatabase(context);
```

For now, we're unable to upload our logo for our Courses as we don't have an **Admin** page to facilitate the feature but what we can do is to save these logos under **wwwroot** and create a folder name **images**.

![](/11_Ecommerce_integration/Discussion/readme-images/3-context-seed/1-logos.png)

## 4. Database Migrations

The Migrations feature enables you to change the data model and deploy your changes to production by updating the database schema without having to drop and re-create the database.

Before we do our Migration we'll configure our **Database String Connection** first by going to **appsettings.json** file in our namespace.

At first, the json file should look like this:

![](/11_Ecommerce_integration/Discussion/readme-images/4-migration/1-connection-json.png)

Now, add your connection string.

```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "LaZuittContext": "Data Source=LaZuitt.db"
  }
}
```

And in our **Program.cs**. Add the following imports and few lines of code.

```c++
using Microsoft.EntityFrameworkCore;

// some other codes

builder.Services.AddDbContext<LaZuittContext>(options =>
{
    options.UseSqlite(builder.Configuration["ConnectionStrings:LaZuittContext"]);
});

// some other codes
```

![](/11_Ecommerce_integration/Discussion/readme-images/4-migration/2-connection-string.png)

Now, that our database connection configuration is done and our DbContext and Models are complete, we will do our Migration.

Open **Tools** > **NuGet Package Manage** > **Package Manager Console (PMC)** and run the following command in the PMC:

* For **Windows Visual Studio** - go to **Tools** > **NuGet Package Manager** > **Package Manager Console**.

* For **Mac Visual Studio** - go to **Views** > **Other Windows** > **Nuget Package Manager Console**.

```
> Add-Migration InitialCreate
```

Or we can use **Command Line Interface** (CLI) but make sure that you are on your Project's namespace directory.

```
$ dotnet tool install -g dotnet-ef
$ dotnet ef migrations add InitialCreate 
```

After running the **Add-Migration** command, files are created under the **Migrations** folder.

Now, we need to update our database with the following command:

**Nuget Package Manager Console**:

```
> Update-Database
```

**Command Line Interface (CLI)**:

```
$ dotnet ef database update
```

So with the **Update-Database**, it generated a **LaZuitt.db** file for us and you can use **DB Browser for SQLite** app to view your database strucutre.

![](/11_Ecommerce_integration/Discussion/readme-images/4-migration/3-database-structure.png)

As you notice here, the **Course Table** doesn't have any record yet just like the other tables.

Now, run your program to execute our **Data Seeding** atleast once.

![](/11_Ecommerce_integration/Discussion/readme-images/4-migration/4-populated-course.png)

Load your database to see the changes.

## 5. Session Extensions

The old version of .NET allows you to use **Session['VariableName']** in your code with ease. But with the newer version like .NET Core 3.1 or .NET 6, it must be implemented through **Session Extensions**.

Session state is accessed from a Razor Pages *PageModel* class or MVC Controller class with **HttpContext.Session**. This property is an **ISession** implementation.

The **ISession** implementation provides several extension methods to set and retrieve integer and string values. The extension methods are in the **Microsoft.AspNetCore.Http** namespace.

All session data must be serialized to enable a distributed cache scenario, even when using the in-memory cache. 

String and integer serializers are provided by the extension methods of **ISession**. Complex types must be serialized by the user using another mechanism, such as **JSON**.

Use the following sample code to serialize objects. 

Add it to your file called **SessionExtension.cs** under **Data** folder.

```cs
using System;
using Newtonsoft.Json;

namespace LaZuittEcommerce.Data;

public static class SessionExtensions
{
    public static void SetJson(this ISession session, string key, object value)
    {
        session.SetString(key, JsonConvert.SerializeObject(value));
    }

    public static T GetJson<T>(this ISession session, string key)
    {
        var sessionData = session.GetString(key);
        return sessionData == null ? default(T) : JsonConvert.DeserializeObject<T>(sessionData);
    }
}   
```

Serialize in **SetJson()** method means converting our objects into a string or any other types that we could actually use and save it to our database. 

Deserialize in **GetJson()** method enables us to recreate objects after they have been serialized for transmission of data for display.

In your **Program.cs** file, we need to add few codes to make our session work.

Use the code below:

Builder.Services section

```cs
builder.Services.AddDistributedMemoryCache();

builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(30);
    options.Cookie.IsEssential = true;
});
```

builder.Build section

```cs
app.UseSession(); // Session
```

Your **program.cs** should look like this.

![](/11_Ecommerce_integration/Discussion/readme-images/5-session/1-session-program.png)

## 6. Course View and Controller

As of now when we run our application, it's still displaying a blank page.

Now, go to **Views/Course/Index.cshtml** to add some content to our Index page.

Use the following code for our Index page but you can customize your website on how you would like it to be.

```html
@model IEnumerable<LaZuittEcommerce.Models.Course>

@{
    ViewData["Title"] = "Enroll Course";
    <style>
        .logo {
            display: block;
            margin: 0 auto;
            width: 150px;
            height: 150px;
        }

        .spacing {
            padding-top: 10px;
        }

        .column {
            float: left;
            width: 25%;
            padding: 0 10px;
            max-height: 100%;
        }

        .card-header, .card-footer {
            background-color: #fff;
        }

        .btn {
            margin-top: 10px;
            margin-bottom: 10px;
            width: 100%;
        }
    </style>
}

<h1 class="pb-4">Enroll Now!</h1>

<div class="row pb-4">
    @foreach (var item in Model)
    {
        <div class="column">
            <div class="card h-100">
                <div class="card-header">
                    @if (item.Logo != null)
                    {
                        <img src="@Url.Content(item.Logo)" class="logo" alt="Logo" />
                    }
                </div>
                <div class="card-body">
                    <h5 class="card-title spacing">@Html.DisplayFor(modelItem => item.Title)</h5>
                    <p class="card-text">@Html.DisplayFor(model => item.Description)</p>
                    <p class="card-text">&#8369;@Html.DisplayFor(model => item.Price)</p>
                </div>
                <div class="card-footer">
                    <a type="submit" class="btn btn-outline-primary" asp-controller="Cart" asp-action="Add" asp-route-id="@item.Id">Add To Cart</a>
                </div>
            </div>
        </div>
    }
</div>   
```

This **@model** directive allows you to access the **Course** that the controller passed to the view by using a Model object that's strongly typed.

```html
@model IEnumerable<LaZuittEcommerce.Models.Course>
```

Internal css styling

```cs
<style> 
    // some code here 
</style>
```

Iterates the course collection and displays it.

```cs
@foreach (var item in Model)
{
    // some code here
}
```

Now, going to **Controllers/CourseController.cs**. Add the following code

```cs
using LaZuittEcommerce.Data;
using Microsoft.AspNetCore.Mvc;

namespace LaZuittEcommerce.Controllers;

public class CourseController : Controller
{
    private readonly LaZuittContext _context;

    public CourseController(LaZuittContext context)
    {
        _context = context;
    }

    public IActionResult Index()
    {
        return View(_context.Course.ToList());
    }
}
```

This method creates our context for us to access the database.

```cs
private readonly LaZuittContext _context;

public CourseController(LaZuittContext context)
{
    _context = context;
}
```

This method returns the Course View in our Index

```cs
public IActionResult Index()
{
    return View(_context.Course.ToList());
}
```

**Course View Page Output**

![](/11_Ecommerce_integration/Discussion/readme-images/6-course/1-course-view.png)

## 7. Cart View and Controllers

In our **Course View** html code, card footer section

```html
<a type="submit" class="btn btn-outline-primary" asp-controller="Cart" asp-action="Add" asp-route-id="@item.Id">Add To Cart</a>
```

Whenever we click this link or it does look like a button, we are targeting the **Add** method in the **Cart** controller, and that method that we will create later on will have an Id parameter.

Let's start with our imports in our **CartController.cs**.

```cs
using LaZuittEcommerce.Data;
using LaZuittEcommerce.Models;
using LaZuittEcommerce.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
```

Just like what we did in our **CourseController**, let's create our context to access our database for our **CartController**.

```cs
private readonly LaZuittContext _context;

public CartController(LaZuittContext context)
{
    _context = context;
}
```

Let's make our table in our Cart Index page

```html
@model LaZuittEcommerce.Models.ViewModels.CartViewModel
@{
    ViewData["Title"] = "Shopping Cart";
    <style>
        .logo {
            display: block;
            margin: 0 auto;
            width: 50px;
            height: 50px;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .borderless td {
            border: none;
        }
    </style>
}

<h2 class="text-center p4">Checkout your Courses</h2>

<table class="table borderless">
    <thead>
        <tr>
            <th colspan="2" class="text-center">Course</th>
            <th>Description</th>
            <th class="text-center">Price</th>
            <th class="text-center">Unit</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach (var item in Model.CartItems)
        {
            <tr>
                <td>
                    @if (item.Icon != null)
                    {
                        <img src="@Url.Content(item.Icon)" alt="Logo" class="logo" />
                    }
                </td>
                <td>
                    @Html.DisplayFor(modelItem => item.Title)
                </td>
                <td>
                    @Html.DisplayFor(modelItem => item.Description)
                </td>
                <td class="text-center">
                    &#8369;@Html.DisplayFor(modelItem => item.Price)
                </td>
                <td class="text-center">
                    @Html.DisplayFor(modelItem => item.Quantity)
                </td>
                <td>
                    <a class="btn btn-outline-danger" id="remove" asp-action="Remove" asp-route-id="@item.CourseId" onclick="">Remove</a>
                </td>
            </tr>
        }
    </tbody>

    <tfoot>
        <tr>
            <th colspan="6" class="text-right">
                Total Amount: &#8369;@Model.TotalAmount.ToString("0.00") <br />
                <button type="button" class="btn btn-secondary mt-2 mb-2" id="place-order" onclick="placeOrder()">Place Order</button>
            </th>
        </tr>
    </tfoot>
</table>
```

Going back to our **CartController.cs**, use the following code for our Index

```cs
public IActionResult Index()
{
    // Create new instance for our CartItem class model
    List<CartItem> cart = HttpContext.Session.GetJson<List<CartItem>>("Cart") ?? new List<CartItem>();

    // Create new instance for our CartViewModel class model
    CartViewModel cartVM = new()
    {
        CartItems = cart,
        TotalAmount = cart.Sum(x => x.Quantity * x.Price)
    };

    // Returns the CartViewModel as View to our Cart/Index.cshtml
    return View(cartVM);
}
```

And when you run the program you shall have this output:

![](/11_Ecommerce_integration/Discussion/readme-images/7-cart/1-table-first-look.png)

Now, we're going to add some functionality to our **Add to Cart** button.

In your **CartController.cs**. Use the following code for our **Add** method.

```cs
public async Task<IActionResult> Add(int id)
{
    // Awaits the Add to Cart click from the user
    Course course = await _context.Course.FindAsync(id);

    // Created a new instance of List<> that acts as our session
    List<CartItem> cart = HttpContext.Session.GetJson<List<CartItem>>("Cart") ?? new List<CartItem>();

    // Search the course based on the passed id parameter
    CartItem cartItem = cart.Where(c => c.CourseId == id).FirstOrDefault();

    // Check if item already exists in our cart
    bool cartItemExists = cart.Any(i => i.CourseId == id);

    // Initial item input
    if (cartItem == null)
    {
        cart.Add(new CartItem(course));
    }

    // Prevents item duplication
    else if (cartItemExists == true)
    {
        TempData.Keep();
        return RedirectToAction("Index", "Course");
    }

    // Initialize the "Cart" session to the value of cart 
    HttpContext.Session.SetJson("Cart", cart);

    // Initialize our TempData["Count"] to the number of items in the cart
    TempData["Count"] = cart.Count();

    // To retain all TempData values
    TempData.Keep();

    // Redirect to Course/Index.cshtml
    return RedirectToAction("Index", "Course");
}
```

Under **Views/Shared/_Layout.cshtml** add the following code for that tiny number in our Cart Navigation Bar that will update if we add or remove an item.

```html
<div class="container">
    <a class="navbar-brand" asp-area="" asp-controller="Course" asp-action="Index">Course</a>
                
    @if (Convert.ToInt32(TempData["Count"]) > 0)
    {
        <a class="navbar-brand text-dange" asp-area="" asp-controller="Cart" asp-action="Index">Cart (@TempData["Count"])</a>
        TempData.Keep();
    }
    else
    {
        <a class="navbar-brand " asp-area="" asp-controller="Cart" asp-action="Index">Cart</a>
    }
</div>
```

TempData variables are string by default. In the **IF** condition, we converted it to **Int** and check its value if greater than 0 and if true then it will return **Cart (number)** otherwise, it's just plain **Cart** text. 

```javascript
@if (Convert.ToInt32(TempData["Count"]) > 0)
```

So right now, we added code for our **Add** method, **Cart/Index** view and **_Layout** for the navigation bar number

If you run the program you should see output like the image below

![](/11_Ecommerce_integration/Discussion/readme-images/7-cart/2-cart-item-count.png)

Now, we're going to add functionality to our **Remove** method in our cart.

As you can see on the Cart Index View razor code

```html
<a class="btn btn-outline-danger" id="remove" asp-action="Remove" asp-route-id="@item.CourseId" onclick="">Remove</a>
```

Everytime you click the Remove button, it's targeting the **Remove** method in our **CartController**.

Use the following code for our **Remove** method.

```cs
public IActionResult Remove(int id)
{
    // Created a new instance of List<> that acts as our session
    List<CartItem> cart = HttpContext.Session.GetJson<List<CartItem>>("Cart");

    // Remove a record based on the Id
    cart.RemoveAll(p => p.CourseId == id);

    // Remove all session if no item exists
    if (cart.Count == 0)
    {
        HttpContext.Session.Remove("Cart");
    }

    // Set our session to existing cart items
    else
    {
        HttpContext.Session.SetJson("Cart", cart);
    }

    // Reassign TempData["Count"] new count value and keep the TempData value
    TempData["Count"] = cart.Count();
    TempData.Keep();

    // Redirect to Cart/Index.cshtml or same page
    return RedirectToAction("Index");
}
```

Now, you can remove items in your cart and that should work perfectly.

But we have a problem here. Everytime we go to our **Cart** it gives us empty table and we do not want that.

So, in our **Cart/Index.cshtml** view. Try the following code and make sure on the **If** condition, everything you want to display is in that condition. Otherwise, if the cart is Empty it will show a Message that **Your Cart is Empty** and with link going back to the Course Index.

```html
@if (Model.CartItems.Count > 0)
{
    // paste your code here
}

else
{
    <h2 class="text-danger text-center pt-5">Your Cart is Empty.</h2>
    <p class="text-center">@Html.ActionLink("Check Available Courses", "Index", "Course")</p>
}
```

**Empty Cart Page** should look like the image below.

![](/11_Ecommerce_integration/Discussion/readme-images/7-cart/3-empty-cart.png)

Now we need to add **Customer Information** form for the checkout and this form will only show once the user click the **Place Order** button.

So this form by default should be hidden.

```html
<div class="row" id="customer-info">
    <div class="col-md-6">
        <h4 class="pb-2 text-right">Customer Information</h4><br />
        <form asp-action="Checkout">
            <div asp-validation-summary="ModelOnly" class="text-danger"></div>
            <div class="form-group" style="display: flex;">
                <input name="FirstName" class="form-control" type="text" placeholder="First name" style="margin-right: 8px" required />
                <input name="LastName" class="form-control" type="text" placeholder="Last name" style="margin-left: 8px;" required />
            </div>
            <div class="form-group">
                <input name="Email" class="form-control" type="email" placeholder="Email" required />
            </div>

            <div class="form-group">
                <input name="BillingAddress" class="form-control" type="text" placeholder="Billing Address" required />
            </div>
            <div class="form-group text-right">
                <input type="submit" value="Checkout" class="btn btn-primary" asp-action="Checkout" onclick="" />
            </div>
        </form>
    </div>
</div>
```

The parent **div** has an **id** called **customer-info**. Now we're going to add our CSS for this form to be hidden and other design of our input field.

In our internal css, add these code below.

```css
#customer-info {
    display: flex;
    justify-content: flex-end;
    visibility: hidden;
}

.form-group input {
    margin-bottom: 15px;
}
```

Our **Place Order** button does have an onclick property calling the function **placeOrder()**.

```html
<button type="button" class="btn btn-secondary mt-2 mb-2" id="place-order" onclick="placeOrder()">Place Order</button>
```

Now let's add a piece of JavaScript code to manipulate and show the div or the form once the button is clicked.

```javascript
<script type="text/javascript">
    function placeOrder() {
        document.getElementById('customer-info').style.visibility = 'visible';
    }
</script>
```

Here's the sample output once **Place Order** button is clicked.

![](/11_Ecommerce_integration/Discussion/readme-images/7-cart/4-customer-form.png)

Now for the last part, we need to add functionality of our **Checkout** feature.

Our checkout button does have a property **asp-action="Checkout"** which it targets the **Checkout** method that we will create later on in our **CartController**.

```html
<input type="submit" value="Checkout" class="btn btn-primary" asp-action="Checkout" />
```

Let's go to **CartController** and add our **Checkout** method.

Use the following code below.

```cs
[HttpPost]
[ValidateAntiForgeryToken]
public async Task<IActionResult> Checkout([Bind("Id,FirstName,LastName,Email,BillingAddress")] Student student)
{
    if (ModelState.IsValid)
    {
        // Save Student record
        _context.Add(student);
        await _context.SaveChangesAsync();

        // Create new instance for Enroll
        Enroll e = new Enroll();
        e.EnrollDate = DateTime.Today;
        // The Id value is from the student that was recently save in our database
        e.StudentId = student.Id;

        // Save Enroll record
        _context.Enroll.Add(e);
        await _context.SaveChangesAsync();

        // Retrieve our cart session data
        List<CartItem> cart = HttpContext.Session.GetJson<List<CartItem>>("Cart");

        // Initialize lastEnrollId from the Enroll Id table that was recently save in our database.
        int lastEnrollId = e.Id;

        foreach (var item in cart)
        {
            EnrollCourses ec = new EnrollCourses();
            ec.EnrollId = lastEnrollId;
            ec.CourseId = item.CourseId;
            ec.Quantity = 1;

            // Save EnrollCourses record
            _context.EnrollCourses.Add(ec);
            _context.SaveChanges();
        }

        // Clear sessions and TempData values         
        TempData.Keep();
        HttpContext.Session.Remove("Cart");
        TempData["Count"] = null;

        // For our partial view
        TempData["Message"] = "Thank you for making purchase with us!";

        // Redirect to Cart/Index.cshtml or same page
        return RedirectToAction("Index");
    }
    return View(student);
}
```

In our **Checkout** method, we are inserting 3 tables upon clicking the button.

As you know Primary Id's are auto incremented when adding a record.

The relationship of Student - Enroll - EnrollCourses:

1. **Student** entry - generates Id
2. **Enroll** entry - requirement (StudentId)
3. **EnrollCourses** entry - requirement (EnrollId)

Let's try to place some order with our user **Jane Doe**.

![](/11_Ecommerce_integration/Discussion/readme-images/7-cart/5-checkout.png)

Added record for our **Student Table**. It generated an **Id** = 1 for Jane Doe.

![](/11_Ecommerce_integration/Discussion/readme-images/7-cart/6-student.png)

Added record for our **Enroll Table**. **StudentId** = 1 is a Foreign Key from **Student Table**.

![](/11_Ecommerce_integration/Discussion/readme-images/7-cart/7-enroll.png)

Added record for our **EnrollCourses Table**. The **EnrollId** is a Foreign Key from 
**Enroll Table**.

![](/11_Ecommerce_integration/Discussion/readme-images/7-cart/8-enroll-courses.png)

Now that we understand these entities relationship to each other. When checking out, it just redirects to the same page. Let's add a simple alert after checkout.

Under **Views/Shared/**, create a partial view, add a new razor view file and name it as **_MessagePartial.cshtml**.

Inside the file, add the following code.

```javascript
@if (TempData["Message"] != null)
{
    <script>
        alert('@Html.Raw(TempData["Message"])')
    </script>
}
```

On the same folder structure, visit **_Layout.cshtml** and on the main tag add the code below.

```html
<partial name="_MessagePartial" />
```

![](/11_Ecommerce_integration/Discussion/readme-images/7-cart/9-partial-message.png)

It shall give you a message like this.

![](/11_Ecommerce_integration/Discussion/readme-images/7-cart/10-partial-alert.png)


# Activity

In this activity, joining table knowledge is needed. Click [here](https://learnsql.com/blog/how-to-join-3-tables-or-more-in-sql/) to learn more about joining tables in SQL.

## 1. Adding History Controller

In your **Controllers** folder, scaffold an empty controller and name it as **HistoryController**

Use the following code for your DbContext

```cs
private readonly LaZuittContext _context;

public HistoryController(LaZuittContext context)
{
    _context = context;
}
```

## 2. Adding OrderHistory Model

In your **Models** folder, create your model class and name it as **OrderHistory**.

Use the following code for your model.

```cs
using System;
namespace LaZuittEcommerce.Models;

public class OrderHistory
{
    public int Id { get; set; }
    public string FullName { get; set; }
    public string Billing { get; set; }
    public DateTime DateEnrolled { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }
}
```

This model serves as a **History View** model as later on we will be accessing this model's data in our **View** page.

## 3. Join Table
In your **HistoryController**. Your code should be like the following.

```cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LaZuittEcommerce.Data;
using LaZuittEcommerce.Models;
using Microsoft.AspNetCore.Mvc;

namespace LaZuittEcommerce.Controllers;

public class HistoryController : Controller
{
    private readonly LaZuittContext _context;

    public HistoryController(LaZuittContext context)
    {
        _context = context;
    }

    public IActionResult Index()
    {
        List<OrderHistory> historyModel = new List<OrderHistory>();

        var orderHistory = (from ec in _context.EnrollCourses

                            join c in _context.Course on ec.CourseId equals c.Id
                            join s in _context.Student on ec.EnrollId equals s.Id
                            join e in _context.Enroll on ec.EnrollId equals e.Id

                            select new
                            {
                                Id = ec.EnrollId,
                                FullName = s.FirstName + " " + s.LastName,
                                Billing = s.BillingAddress,
                                DateEnrolled = e.EnrollDate,
                                Title = c.Title,
                                Description = c.Description,
                                Price = c.Price,
                                Quantity = c.Quantity
                            }).ToList();

        foreach (var item in orderHistory)

            historyModel.Add(new OrderHistory()
            {
                Id = item.Id,
                FullName = item.FullName,
                Billing = item.Billing,
                DateEnrolled = item.DateEnrolled,
                Title = item.Title,
                Description = item.Description,
                Price = item.Price,
                Quantity = item.Quantity
            });

        return View(historyModel);
    }
}
```

## 4. View Page

Use the following code for your **Index** page.

```html
@model IEnumerable<EcommerceDemo.Models.OrderHistory>

<h1>Order History</h1>

@foreach (var item in Model)
{
    <ul class="list-group">
        <li class="list-group-item active">@item.FullName - @item.DateEnrolled - @item.Billing</li>
        <li class="list-group-item">Course Title: <strong> @item.Title </strong></li>
        <li class="list-group-item">@item.Description</li>
        <li class="list-group-item">Price: P @item.Price (Unit: @item.Quantity)</li> 
    </ul> <br />
}
```

And lastly in your **Views/Shared/_Layout.cshtml**. Add your **History** link in the navigation bar.

Use the following code.

```html
<a class="navbar-brand mr-auto" asp-area="" asp-controller="History" asp-action="Index">History</a>
```

**Output**:

![](/11_Ecommerce_integration/Discussion/readme-images/8-join-activity/1-activity-output.png)


