using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FinalTestChar.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Text.Json;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace FinalTestChar.Controllers
{
    public class CourseController : Controller
    {
        private readonly LaZuittContext _context;

        public CourseController(LaZuittContext context)
        {
            _context = context;
        }

        public int[] idList = { };
        private int[] fromPostId = { };

        public int[] pubDummy = { 2, 1, 3 };

        // Retreive and display Courses on the Index page
        public async Task<IActionResult> Index()
        {

            //TempData["count"] = idList.Count();
            TempData.Keep();

            return View(await _context.Course.ToListAsync());

        }



        // Retrieve course by Id
        public ActionResult AddToCart(int id)
        {
            var query = _context.Course.Where(i => i.Id == id).FirstOrDefault();
            return View(query);
        }

        // Redirect to Course/Index.cshtml after clicking Add To Cart button

        [HttpPost]
        public ActionResult AddToCart()
        {
            return RedirectToAction("Index");
        }

        // Display items at the checkout page based on Id's save in the local storage

        public ActionResult EmptyCart()
        {
            return View();
        }

        public ActionResult Checkout()
        {
            return View();
            //return RedirectToAction("_CheckoutCart", new { values = "1,2"});
        }

        [HttpGet]
        [Route("[controller]/CheckoutCart")]
        public ActionResult _MyCart(string values)
        {
            Console.WriteLine("this is from get result: " + JsonConvert.SerializeObject(values)); // "1, 2, 3"

            int[] dummyValues = { 2, 3, 1 };

            string value = String.Join(", ", values);
            string newValue = new string(value); // 1, 2, 3

            int[] ids = newValue.Select(i => i - '0').ToArray(); // string to int[]


            Console.WriteLine("converted: " + newValue);


            var course = _context.Course.Where(x => ids.Contains(x.Id)).ToList();

            var res = JsonConvert.SerializeObject(course);
            Console.WriteLine(res);
            return Json(res);

        }

    }
}